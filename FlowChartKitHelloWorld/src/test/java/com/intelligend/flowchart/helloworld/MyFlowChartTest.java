package com.intelligend.flowchart.helloworld;

import java.util.ArrayList;
import java.util.List;

import com.intelligend.flowchart.FlowChart;
import com.intelligend.flowchart.FlowChartResult;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple MyFlowChart.
 */
public class MyFlowChartTest extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MyFlowChartTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MyFlowChartTest.class );
    }

    public void testOne() throws Exception
    {
    	List<String> expectedChain = new ArrayList<String>();
		MyFlowChartDTO dto = new MyFlowChartDTO();
		FlowChart<MyFlowChartDTO> chart = new FlowChart<MyFlowChartDTO>("com.intelligend.flowchart.helloworld.nodes.StartNode", 10);
		FlowChartResult ret = null;
		
		expectedChain.add("com.intelligend.flowchart.helloworld.nodes.StartNode");
		expectedChain.add("com.intelligend.flowchart.helloworld.nodes.EndNode");
		
		dto.setOrderAmountHistory(1000);
		ret = chart.executeFlowChart(dto, null);
		System.out.println("Jump chain: " + ret.getNodeJumps().toString());

		assertTrue(expectedChain.equals(ret.getNodeJumps()));
		
    }

    public void testTwo() throws Exception
    {
    	List<String> expectedChain = new ArrayList<String>();
		MyFlowChartDTO dto = new MyFlowChartDTO();
		FlowChart<MyFlowChartDTO> chart = new FlowChart<MyFlowChartDTO>("com.intelligend.flowchart.helloworld.nodes.StartNode", 10);
		FlowChartResult ret = null;
		
		expectedChain.add("com.intelligend.flowchart.helloworld.nodes.StartNode");
		expectedChain.add("com.intelligend.flowchart.helloworld.nodes.ThankCustomerNode");
		expectedChain.add("com.intelligend.flowchart.helloworld.nodes.EndNode");
		
		dto.setOrderAmountHistory(2000);
		ret = chart.executeFlowChart(dto, null);
		System.out.println("Jump chain: " + ret.getNodeJumps().toString());

		assertTrue(expectedChain.equals(ret.getNodeJumps()));
		
    }
    
    
}
