package com.intelligend.flowchart.helloworld;

import com.intelligend.flowchart.FlowChart;

/**
 * Hello world!
 *
 */
public class MyFlowChart 
{
    public static void main( String[] args )
    {
    	try{
    		String graphvizBinDir = "set your graphviz bin directory here";
    		String chartImageFile = "set the flowchart image path and filename here";
    		FlowChart<MyFlowChartDTO> chart = new FlowChart<MyFlowChartDTO>("com.intelligend.flowchart.helloworld.nodes.StartNode", 10);
       		chart.generateFlowChartImage(graphvizBinDir, chartImageFile, "com.intelligend.flowchart.helloworld.nodes");
    		System.out.println("Test paths: " + chart.getTestPaths());
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
}
