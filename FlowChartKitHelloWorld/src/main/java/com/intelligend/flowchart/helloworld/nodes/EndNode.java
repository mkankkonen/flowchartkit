package com.intelligend.flowchart.helloworld.nodes;

import java.util.Map;

import com.intelligend.flowchart.FlowChartException;
import com.intelligend.flowchart.FlowChartNode;
import com.intelligend.flowchart.FlowChartResult;
import com.intelligend.flowchart.annotation.NextFlowChartNodes;
import com.intelligend.flowchart.helloworld.MyFlowChartDTO;

public class EndNode implements FlowChartNode<MyFlowChartDTO> {

	@Override
	@NextFlowChartNodes(
			nodePackage = "com.intelligend.flowchart.helloworld.nodes",
			nextNode = {},
			nextNodeCondition = {}
			)
	public FlowChartResult executeNode(MyFlowChartDTO arg0, Map<String, Object> arg1) throws FlowChartException {
		FlowChartResult ret = new FlowChartResult();
		return ret;
	}

}
