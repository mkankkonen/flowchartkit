package com.intelligend.flowchart.helloworld.nodes;

import java.util.Map;

import com.intelligend.flowchart.FlowChartException;
import com.intelligend.flowchart.FlowChartNode;
import com.intelligend.flowchart.FlowChartResult;
import com.intelligend.flowchart.annotation.NextFlowChartNodes;
import com.intelligend.flowchart.helloworld.MyFlowChartDTO;

public class StartNode implements FlowChartNode<MyFlowChartDTO> {

	@Override
	@NextFlowChartNodes(
			nodePackage = "com.intelligend.flowchart.helloworld.nodes",
			nextNode = {"ActivateSleepingNode", "ThankCustomerNode", "EndNode"},
			nextNodeCondition = {"0 <= sales < 1000", "sales >= 1000", "sales < 0"}
			)
	public FlowChartResult executeNode(MyFlowChartDTO arg0, Map<String, Object> arg1) throws FlowChartException {
		FlowChartResult ret = new FlowChartResult();
		if(arg0.getOrderAmountHistory() < 1000 && arg0.getOrderAmountHistory() > 0){
			ret.setNextNode("com.intelligend.flowchart.helloworld.nodes.ActivateSleeingNode");
		}else if(arg0.getOrderAmountHistory() > 1000){
			ret.setNextNode("com.intelligend.flowchart.helloworld.nodes.ThankCustomerNode");
		}else{
			ret.setNextNode("com.intelligend.flowchart.helloworld.nodes.EndNode");
		}
		return ret;
	}

}
