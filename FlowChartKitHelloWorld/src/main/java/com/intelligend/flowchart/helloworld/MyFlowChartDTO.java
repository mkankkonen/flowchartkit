package com.intelligend.flowchart.helloworld;

public class MyFlowChartDTO  {

	private int orderCountHistory = 0;
	private double orderAmountHistory = 0;
	
	
	public int getOrderCountHistory() {
		return orderCountHistory;
	}
	public void setOrderCountHistory(int orderCountHistory) {
		this.orderCountHistory = orderCountHistory;
	}
	public double getOrderAmountHistory() {
		return orderAmountHistory;
	}
	public void setOrderAmountHistory(double orderAmountHistory) {
		this.orderAmountHistory = orderAmountHistory;
	}
	
	
	
}
