package com.intelligend.flowchart.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NextFlowChartNodes {

	public String nodePackage() default "";
	public String[] nextNode() default {};
	public String[] nextNodeCondition() default {};
	
}
