package com.intelligend.flowchart;

import java.util.Map;

public interface FlowChartNode<E> {

	public FlowChartResult executeNode(E dataObject, Map<String, Object> globalMap) throws FlowChartException;
	
}
