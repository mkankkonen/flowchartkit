package com.intelligend.flowchart;

public class FlowChartException extends Exception {

	private static final long serialVersionUID = 1L;

	public FlowChartException(String errMessage){
		super(errMessage);
	}

	public FlowChartException(Exception e){
		super(e);
	}
	
}
