package com.intelligend.flowchart;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.intelligend.flowchart.annotation.NextFlowChartNodes;

public class FlowChart<E>
{
	private final boolean prodVersion = true;
	private final String notSupportedText = "This feature is not supported in free version.";
	
	private String startNode = null;
	private int maxJumps = 10;
	private List<String> testPaths = new ArrayList<String>();
	private List<String> nonStartNodes = new ArrayList<String>();
	private Map<String, List<String>> hNodes = new HashMap<String, List<String>>();
//	private boolean printStats = true;
	
	public FlowChart(String startNode, int maxJumps){
		this.startNode = startNode;
		this.maxJumps = maxJumps;
		String parts[] = startNode.split("\\.");
		//testPaths.add(parts[parts.length - 1] + "/");
		this.addToTestPaths(parts[parts.length - 1], null);

		if(!prodVersion){
			try{
				int jumpCount = 0;
				String hintText = null;
				String nodePackage = startNode.substring(0, startNode.lastIndexOf("."));
				this.generateFlowChartImage(null, null, nodePackage);
				jumpCount = this.getTestPathsCount();
				System.out.println("Possible node jumps count: " + jumpCount);
				
				if(jumpCount >= 10){
					hintText = "For this FlowChart implementation you need " + jumpCount + " test cases to cover all possible jumps.\n";
				}
				
				if(hintText != null){
					System.out.println(hintText);
				}
				
			}catch(Exception e){
			}
		}
		
	}

	private synchronized void appendChildren(String node, List<String> child, List<String> newTestPathsParam){
//		System.out.println("appendChildren: " + node + " " + child);
//		System.out.println("   testPaths: " + testPaths);
		List<String> newTestPaths = new ArrayList<String>();
		
		if(newTestPathsParam != null){
			newTestPaths = newTestPathsParam;
		}
		
		if(testPaths.size() == 0){
			for(int i = 0; i < child.size(); i++){
				newTestPaths.add(node + "/" + child.get(i));
				testPaths = new ArrayList<String>(newTestPaths);
				if(hNodes.get(child.get(i)) != null){
					appendChildren(child.get(i), hNodes.get(child.get(i)), newTestPaths);
				}
			}
		}else{
			for(int i = 0; i < testPaths.size(); i++){
				String path = testPaths.get(i);
//				System.out.println(path + " ends with " + "/" + node);
				if(path.endsWith("/" + node)){
//					System.out.println("   REMOVE PATH: " + path);
					newTestPaths.remove(path);
					for(int ii = 0; ii < child.size(); ii++){
//						System.out.println("    yes it ends! adding: " + path + "/" + child.get(ii));
						newTestPaths.add(path + "/" + child.get(ii));
						testPaths = new ArrayList<String>(newTestPaths);
						if(hNodes.get(child.get(ii)) != null){
							appendChildren(child.get(ii), hNodes.get(child.get(ii)), newTestPaths);
						}
					}
				}else{
					if(!newTestPaths.contains(path)){
						newTestPaths.add(path);
					}
				}
			}
		}
		
//		testPaths = new ArrayList<String>(newTestPaths);
	}
	
	public List<String> getTestPaths(){
		testPaths = new ArrayList<String>();
		String parts[] = startNode.split("\\.");
		appendChildren(parts[parts.length - 1], hNodes.get(parts[parts.length - 1]), null);
		return this.testPaths;
	}

	
	private int getTestPathsCount(){
		if(testPaths.size() == 0){
			getTestPaths();
		}
		return this.testPaths.size();
	}

	private void addToTestPaths(String node, String nextNode){
//		System.out.println("addToTestPaths: " + node + ", " + nextNode);
		List<String> child = hNodes.get(node);
		if(child == null){
			child = new ArrayList<String>();
			hNodes.put(node, child);
		}
		if(nextNode != null){
			child.add(nextNode);
		}
	}
	
/*	
	private void x_addToTestPaths(String node, String nextNode){
		System.out.println("addToTestPaths: " + node + ", " + nextNode);
		List<String> workArray = new ArrayList<String>(testPaths);
		List<String> newTestPaths = new ArrayList<String>();
		
		if(node != null && nextNode != null){
			boolean nodeFound = false;
			for(int i = 0; i < workArray.size(); i++){
				String path = workArray.get(i);
				if(path.endsWith("/" + node)){
					path = path + "/" + nextNode;
					System.out.println("  addNewTestPath(found): " + path);
					newTestPaths.add(path);
					nodeFound = true;
				}else{
//					System.out.println("  addNewTestPath(found): " + path);
					System.out.println("  addNewTestPath(not found): " + path);
//					newTestPaths.add(path);
					this.nonStartNodes.add(node + "/" + nextNode);
				}
			}
		
			if(!nodeFound && this.startNode.endsWith(node)){
				newTestPaths.add(node + "/" + nextNode);
			}else{
				
				//if(!nodeFound){
				//	this.nonStartNodes.add(node + "/" + nextNode);
				//}
				
			}
		}else{
			newTestPaths = new ArrayList<String>(workArray);
		}
		
		List<String> newNonStartNodes = new ArrayList<String>();
		workArray = new ArrayList<String>(newTestPaths);
		for(int i = 0; i < nonStartNodes.size(); i++){
			String path = nonStartNodes.get(i);
			System.out.println("Finding location for non start node: " + path);
			String[] p = path.split("/");
			String node2 = p[0];
			String nextNode2 = p[1];
			boolean bFound = false;

			for(int ii = 0; ii < workArray.size(); ii++){
				String path2 = workArray.get(ii);
				System.out.println("     is mactch?: " + path2);
				if(path2.endsWith("/" + node2)){
					path2 = path2 + "/" + nextNode2;
					newTestPaths.add(path2);
					bFound = true;
				}
			}
			if(!bFound){
				newNonStartNodes.add(path);
			}
			
		}

		nonStartNodes = new ArrayList<String>(newNonStartNodes);
		testPaths = new ArrayList<String>(newTestPaths);
	}
*/
	
    private List<Class> getClasses(ClassLoader cl, String pack) throws Exception{

        //String dottedPackage = pack.replaceAll("[/]", ".");
        String dottedPackage = pack;
        List<Class> classes = new ArrayList<Class>();
        URL upackage = cl.getResource(pack.replaceAll("\\.", "/"));

        DataInputStream dis = new DataInputStream((InputStream) upackage.getContent());
        String line = null;
        while ((line = dis.readLine()) != null) {
            if(line.endsWith(".class")){
               classes.add(Class.forName(dottedPackage+"."+line.substring(0,line.lastIndexOf('.'))));
            }
        }
        return classes;
    }	

    private void writeImage(String graphvizBinPath, String pngSaveFile, String dotFileContent) throws Exception {

		if(!prodVersion){
			return;
		}
    	
    	if(graphvizBinPath == null || pngSaveFile == null){
    		System.out.println("Graphviz and/or png file path cannot be null.");
    		return;
    	}
    	
    	
    	String command = graphvizBinPath + "/dot -Tpng " + pngSaveFile + ".dot -o" + pngSaveFile;
    	File f = new File(pngSaveFile + ".dot");
    	//FileOutputStream fOut = new FileOutputStream("C:/miki/personal/intelligend/mvnprojects/flowchartdemo/FlowChartEnv/bin/a.dot");
    	FileOutputStream fOut = new FileOutputStream(pngSaveFile + ".dot");
    	fOut.write(dotFileContent.getBytes());
    	fOut.flush();
    	fOut.close();
    	
//    	ProcessBuilder pb = new ProcessBuilder(command);
    	Runtime.getRuntime().exec(command).waitFor();
    	f.delete();
    }
    
	public void generateFlowChartImage(String graphvizBinPath, String pngSaveFile, String nodePackage) throws FlowChartException {

		/*
		if(!prodVersion && (graphvizBinPath != null || pngSaveFile != null)){
			System.out.println(notSupportedText);
			return;
		}
		*/
		
		try{
			List<Class> classes = this.getClasses(this.getClass().getClassLoader(), nodePackage);
			Iterator<Class> iter = classes.iterator();
			StringBuilder sb = new StringBuilder();
			sb.append("digraph Intelligend {\n");
			
			while(iter.hasNext()){
				Class obj = iter.next();
				for(Method method : obj.getDeclaredMethods()){
					if(method.getName().equals("executeNode")){
						NextFlowChartNodes annotation = method.getAnnotation(NextFlowChartNodes.class);
						String currentNode = obj.getName();
						String[] parts = currentNode.split("\\.");
						currentNode = parts[parts.length - 1];
						if(annotation != null){
							int i = 0;
							boolean printCondition = true;
							String nodePackageAnnotation = annotation.nodePackage();
							String[] nextNodes = annotation.nextNode();
							String[] nextNodesConditions = annotation.nextNodeCondition();
							String classCheckPackage = nodePackageAnnotation;
														
							if(!nodePackageAnnotation.trim().equals("")){
								nodePackageAnnotation = nodePackageAnnotation.trim() + ".";
							}else{
								nodePackageAnnotation = nodePackageAnnotation.trim();
							}
							// dots are not allowed in .dot files - this will also make the graph more nice without packages in names
							nodePackageAnnotation = "";
							
							//System.out.println(obj.getName() + " -> " + method.getName());
							
							if(nextNodes.length != nextNodesConditions.length){
								System.out.println(" WARNING! nextNodes and nextNodesContion lengths are not equal:  " + nextNodes.length + " != " + nextNodesConditions.length);
								printCondition = false;
							}
							
							//System.out.println("  nextNodes: ");
							for(String s : nextNodes){
								String err = "";
								String condition = "";
								try{
									Class.forName(classCheckPackage + "." + s.trim());
								}catch(Exception e){
									System.out.println(" WARNING! Class not found: " + classCheckPackage + "." + s.trim());
									err = " Class not found! ";
								}
								if(printCondition){
									condition = nextNodesConditions[i];
								}
								//System.out.println("   " + nodePackageAnnotation + s.trim() + " (" + condition + ") " + err);
								// LR_0 -> LR_2 [ label = "SS(B)" ];
								sb.append(currentNode);
								sb.append(" -> ");
								sb.append(nodePackageAnnotation + s.trim() + " [label = \"" + condition + " (" + err + ")\"];\n");
								this.addToTestPaths(currentNode, s.trim());
								i++;
							}
						}
					}
				}
			}

			sb.append("}");
			
			// add nonStartNodes still not added
			//this.addToTestPaths(null, null);
			
			writeImage(graphvizBinPath, pngSaveFile, sb.toString());

		}catch(Exception e){
			throw new FlowChartException(e);
		}
	}
	
	public FlowChartResult executeFlowChart(E dataObject, Map<String, Object> globalMap) throws FlowChartException {
		FlowChartResult res = new FlowChartResult();
		try{
			
			int depthCounter = 0;
			FlowChartNode<E> nextNode = (FlowChartNode<E>)Class.forName(startNode).newInstance();
			String nextNodeClass = null;
			FlowChartResult resBuf = null;
			Iterator<String> iter = null;
			
			res.addNodeJump(startNode);
			
			while((resBuf = nextNode.executeNode(dataObject, globalMap)).getNextNode() != null){
				nextNodeClass = resBuf.getNextNode();
				nextNode = (FlowChartNode<E>)Class.forName(nextNodeClass).newInstance();
				res.addNodeJump(nextNodeClass);
				iter = resBuf.getReturnValues().keySet().iterator();
				
				while(iter.hasNext()){
					String key = iter.next();
					Object value = resBuf.getReturnValue(key);
					res.setReturnValue(key, value);
				}
				depthCounter++;
				
				if(depthCounter >= maxJumps){
					throw new FlowChartException("Node jump max exceed: " + depthCounter + " >= " + maxJumps);
				}
			}
			
			iter = resBuf.getReturnValues().keySet().iterator();
			while(iter.hasNext()){
				String key = iter.next();
				Object value = resBuf.getReturnValue(key);
				res.setReturnValue(key, value);
			}

			
		}catch(Exception e){
			throw new FlowChartException(e);
		}
		return res;
	}
	
}
