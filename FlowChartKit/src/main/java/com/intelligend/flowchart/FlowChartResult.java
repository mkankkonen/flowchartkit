package com.intelligend.flowchart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FlowChartResult {

	private List<String> nodeJumps = new ArrayList<String>();
	private String nextNode = null;
	private Map<String, Object> returnValues = new HashMap<String, Object>();
	
	public void addNodeJump(String node){
		nodeJumps.add(node);
	}
	
	public List<String> getNodeJumps(){
		return this.nodeJumps;
	}
	
	public void setNextNode(String nextNode){
		this.nextNode = nextNode;
	}
	
	public String getNextNode(){
		return this.nextNode;
	}
	
	public void setReturnValue(String key, Object value){
		this.returnValues.put(key, value);
	}
	
	public Object getReturnValue(String key){
		return this.returnValues.get(key);
	}
	
	public Map<String, Object> getReturnValues(){
		return this.returnValues;
	}
	
}
