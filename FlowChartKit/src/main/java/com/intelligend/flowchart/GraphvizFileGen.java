package com.intelligend.flowchart;

import java.io.FileInputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class GraphvizFileGen {

	private List<String> getJarClasses(String jarFile) throws Exception {
		List<String> classes = new ArrayList<String>();
		FileInputStream fIn = new FileInputStream(jarFile);
		ZipInputStream zip = new ZipInputStream(fIn);
		ZipEntry zipEntry = null;
		while((zipEntry = zip.getNextEntry()) != null){
			String name = zipEntry.getName();
			if(name.endsWith(".class")){
				name = name.replace(".class", "");
				name = name.replace('/', '.');
				classes.add(name);
				System.out.println(name);
			}
		}
		zip.close();
		return classes;
	}
	
	private String getClassGraphvizBlock(String className) throws Exception {
		StringBuilder sb = new StringBuilder();
		Class<?> obj = Class.forName(className);
		
		for(Method method : obj.getMethods()){
			System.out.println("method annotation count: " + method.getName() + " / " + method.getAnnotations().length);
			for(Annotation annotation : method.getAnnotations()){
				System.out.println(annotation.toString());
			}
		}
		
		return sb.toString();
	}
	
	public void generateGraphvizFile(String jarFile) throws Exception {
		System.out.println("Generating Graphviz file from jar: " + jarFile);
		List<String> classes = this.getJarClasses(jarFile);
		Iterator<String> iter = classes.iterator();
		StringBuilder sb = new StringBuilder();
		while(iter.hasNext()){
			String clazz = iter.next();
			sb.append(this.getClassGraphvizBlock(clazz));
		}
	}
	
	public static void main(String[] args){
		System.out.println("Intelliend Graphviz file generator v0.1b. Copyright(c) 2014 vNetCon Oy");
		System.out.println("GraphvizFileGen started...");
		try{
			GraphvizFileGen gfg = new GraphvizFileGen();
			String jarFile = null;
			if(args.length > 0){
				jarFile = args[0];
			}else{
				jarFile = "./chartNodes/flowchart.jar";
			}
			gfg.generateGraphvizFile(jarFile);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("GraphvizFileGen done.");
	}
	
}
